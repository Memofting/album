import os
import sys
from PyQt5 import QtWidgets, QtCore, QtGui
from Album import Album


class AlbumWidget(QtWidgets.QWidget):
    """
    Класс, отражающий функциональный элемент альбома на главном
    окне.
    """
    ICON_PATH = os.path.join(".", "images", "icons", "album.png")
    clicked = QtCore.pyqtSignal(Album)

    def __init__(self, album_name, parent=None):
        super().__init__(parent)
        if isinstance(album_name, Album):
            self.album = album_name
        elif isinstance(album_name, str):
            self.album = Album(album_name)
        self.album_name = self.album.name
        self.image = QtWidgets.QLabel()
        self.image.setPixmap(QtGui.QPixmap(self.ICON_PATH))

        self.name = QtWidgets.QLabel(self.album_name)
        self.name.setAlignment(QtCore.Qt.AlignCenter)

        self.vbox = QtWidgets.QVBoxLayout()
        self.vbox.addWidget(self.name)
        self.vbox.addWidget(self.image)
        self.vbox.addStretch(1)

        self.setLayout(self.vbox)
        self.resize(300, 300)

    def mouseDoubleClickEvent(self, a0: QtGui.QMouseEvent):
        self.clicked.emit(self.album)


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    form = QtWidgets.QVBoxLayout()
    hbox = QtWidgets.QHBoxLayout()
    hbox.addLayout(form)
    hbox.addStretch(1)
    widget = QtWidgets.QWidget()
    widget.setLayout(hbox)

    label = AlbumWidget("LocalAlbum")
    name = QtWidgets.QLabel(os.path.split(label.album.dir_name)[1])
    name.setAlignment(QtCore.Qt.AlignCenter)
    form.addWidget(label)
    form.addWidget(name)
    form.addStretch(1)
    widget.setGeometry(300, 300, 400, 500)
    widget.show()
    sys.exit(app.exec())
