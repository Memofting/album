#!usr/bin/env python3
from PyQt5 import QtCore, QtGui
from DBAlbum import AlbumDataBase

EXTENSION = {
    image_format.data().decode('utf-8').upper()
    for image_format in QtGui.QImageReader.supportedImageFormats()}


class Album:
    """
    Класс который отвечает сущности альбома. В инициализаторе он принимает
    свое уникальное имя, и имя родительской базы данных.
    """
    def __init__(self, name, parent_database: AlbumDataBase):
        """
        При инициализации мы можем либо загрузить уже имеющийся альбом, либо
        создать новый.
        """
        self.name = name
        self.parent_database = parent_database

    def get_info(self):
        whole_db_data = self.parent_database.get_albums_info().get(
            self.name, {
                AlbumDataBase.ORDER_NAME: list(),
                AlbumDataBase.IMAGES_NAME: dict()})
        return {
            AlbumDataBase.ORDER_NAME: whole_db_data[AlbumDataBase.ORDER_NAME],
            AlbumDataBase.IMAGES_NAME: whole_db_data[AlbumDataBase.IMAGES_NAME]
        }

    @QtCore.pyqtSlot(str)
    def add_image(self, full_image_name, image_bytes: bytes, ext="JPG"):
        self.parent_database.add_image(
            image_name=full_image_name,
            albums_name=self.name,
            image_bytes=image_bytes,
            ext=ext
        )

    @QtCore.pyqtSlot(str)
    def remove_image(self, full_image_name):
        self.parent_database.remove_image(
            image_name=full_image_name,
            albums_name=self.name)

    @QtCore.pyqtSlot()
    def clear(self):
        self.parent_database.clear(albums_name=self.name)

    @QtCore.pyqtSlot()
    def merge(self, other_albums_db: AlbumDataBase):
        self.parent_database.merge(other_albums_db)

    @QtCore.pyqtSlot()
    def delete(self):
        self.clear()
        pass
