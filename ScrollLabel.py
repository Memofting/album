#!/usr/bin/env python3
from PyQt5 import QtCore, QtWidgets, QtGui
import sys


class BackShadow(QtWidgets.QGraphicsDropShadowEffect):
    """
    Прописывает графику для ScrollArea.
    """
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setBlurRadius(5)
        self.setXOffset(5)
        self.setYOffset(5)
        # gray color
        self.setColor(QtGui.QColor("#D3D3D3"))


class MyLabel(QtWidgets.QLabel):
    """Класс, отвечающий функционалу фотографии на scroll_area. По клику
    посылает сигнал clicked[Qlabel], который содержит иформацию о фотографии,
    которая была выбрана. При наведении курсора на область генерируется
    сигнал entered, при уводе указателя мыши с области left.
    """

    clicked = QtCore.pyqtSignal(QtWidgets.QLabel)
    entered = QtCore.pyqtSignal(QtWidgets.QLabel)
    left = QtCore.pyqtSignal(QtWidgets.QLabel)

    def __init__(self, image, image_name="unknown",
                 w=120, h=120, parent=None):
        super().__init__(parent)

        self.name = image_name
        self.setPixmap(QtGui.QPixmap.fromImage(image))
        self.setAlignment(QtCore.Qt.AlignCenter)
        # настройка теней
        self.setGraphicsEffect(BackShadow())
        self.resize(w, h)

    def mouseDoubleClickEvent(self, event):
        self.clicked.emit(self)
