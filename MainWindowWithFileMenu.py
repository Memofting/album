#!/usr/bin/env python3

import os
from PyQt5 import QtWidgets, QtCore, QtGui
from ImageGetter import EXTENTIONS
from saveDialog import SaveMessageBox


class FileDialog(QtWidgets.QFileDialog):
    def __init__(self, parent: QtWidgets.QMenu, file_mode, name_filter):
        super().__init__(parent)
        self.setFileMode(file_mode)
        self.setNameFilter(name_filter)
        self.setViewMode(QtWidgets.QFileDialog.Detail)
        self.setDirectory('~/')


class FileAction(QtWidgets.QAction):
    def __init__(self, parent: QtWidgets.QMenu,
                 icon_path, name, short_cut,
                 status_tip, trigger_handler):
        super().__init__(QtGui.QIcon(icon_path), name, parent)
        self.setShortcut(short_cut)
        self.setStatusTip(status_tip)
        self.triggered.connect(trigger_handler)


def translate_to_norm_view(paths):
    return list(map(os.path.normpath, paths))


class FileMenu(QtWidgets.QMenu):

    open_dir_event = QtCore.pyqtSignal(list)
    add_dir_event = QtCore.pyqtSignal(list)

    open_files_event = QtCore.pyqtSignal(list)
    add_files_event = QtCore.pyqtSignal(list)

    def __init__(self, parent: QtWidgets.QMainWindow):
        super().__init__(parent)
        open_dir_icon = os.path.join(".", "images", "icons", "open_folder.png")
        self.open_dir_action = (
            FileAction(self, open_dir_icon,
                       "Open folder", "Ctrl+O",
                       "Open folder as new album.",
                       self.show_open_dir_dialog))
        add_dir_icon = os.path.join(".", "images", "icons", "add_folder.png")
        self.add_dir_action = (
            FileAction(self, add_dir_icon,
                       "Add folder", "Ctrl+A",
                       "Add folders content to current album.",
                       self.show_add_dir_dialog))
        open_file_icon = os.path.join(".", "images", "icons", "open_file.png")
        self.open_files_action = (
            FileAction(self, open_file_icon,
                       "Open files", "Ctrl+Shift+O",
                       "Open files as new album.",
                       self.show_open_files_dialog))
        add_file_icon = os.path.join(".", "images", "icons", "add_file.png")
        self.add_files_action = (
            FileAction(self, add_file_icon,
                       "Add files", "Ctrl+Shift+A",
                       "Add files to current album.",
                       self.show_add_files_dialog))
        self.exit_action = QtWidgets.QAction(
            QtGui.QIcon("./images/icons/exit.ico"),
            "&Exit", self)
        self.exit_action.setShortcut("Ctrl+Q")
        self.exit_action.setStatusTip("Exit from the application.")
        self.exit_action.triggered.connect(QtWidgets.qApp.exit)

        self.setTitle("&File")
        self.addAction(self.open_dir_action)
        self.addAction(self.add_dir_action)
        self.addAction(self.open_files_action)
        self.addAction(self.add_files_action)
        self.addAction(self.exit_action)

        image_ext = ["*" + ext for ext in EXTENTIONS]
        image_filter = "Images ({})".format(" ".join(map(str, image_ext)))

        self.dir_dialog = FileDialog(
            self, QtWidgets.QFileDialog.Directory,
            name_filter=image_filter)
        self.file_dialog = FileDialog(
            self, QtWidgets.QFileDialog.ExistingFiles,
            name_filter=image_filter)

    @staticmethod
    def show_dialog(dialog: QtWidgets.QFileDialog, event: QtCore.pyqtSignal):
        if dialog.exec_():
            file_names = translate_to_norm_view(
                dialog.selectedFiles())
            event.emit(file_names)

    def show_open_dir_dialog(self):
        self.show_dialog(self.dir_dialog, self.open_dir_event)

    def show_add_dir_dialog(self):
        self.show_dialog(self.dir_dialog, self.add_dir_event)

    def show_add_files_dialog(self):
        self.show_dialog(self.file_dialog, self.add_files_event)

    def show_open_files_dialog(self):
        self.show_dialog(self.file_dialog, self.open_files_event)


class AlbumMenu(QtWidgets.QMenu):

    open_event = QtCore.pyqtSignal(str)
    add_event = QtCore.pyqtSignal(list)
    save_event = QtCore.pyqtSignal(str)
    clear_event = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.save_message_box = SaveMessageBox()
        self.save_message_box.path_for_saving_event.connect(self.save_database)
        self.file_dialog = FileDialog(
            self, QtWidgets.QFileDialog.ExistingFiles,
            name_filter="Database files (*.dat)")

        open_icon = os.path.join(".", "images", "icons", "open_folder.png")
        self.open_action = (
            FileAction(self, open_icon,
                       "Open album", "Alt+O",
                       "Open album with entered name.",
                       self.open_dialog))

        add_icon = os.path.join(".", "images", "icons", "add_folder.png")
        self.add_action = (
            FileAction(self, add_icon,
                       "Add database", "Alt+N",
                       "Add other database entities to current database.",
                       self.add_dialog))
        save_icon = add_icon
        self.save_action = (
            FileAction(self, save_icon,
                       "Save database", "Alt+S",
                       "Save current database to file, "
                       "pointed in file dialog.",
                       self.save_dialog)
        )
        clear_icon = add_icon
        self.clear_action = (
            FileAction(self, clear_icon,
                       "Clear database", "Alt+C",
                       "Clear current database.",
                       self.clear_dialog)
        )
        self.le = QtWidgets.QLineEdit(self)
        qr = self.le.frameGeometry()
        cp = QtWidgets.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.le.move(qr.topLeft())

        self.setTitle("&Albums")
        self.addAction(self.open_action)
        self.addAction(self.add_action)
        self.addAction(self.save_action)
        self.addAction(self.clear_action)

    def open_dialog(self):
        text, ok = QtWidgets.QInputDialog.getText(
            self, 'Open dialog', "Enter album's name:")

        if ok:
            self.le.setText(str(text))
            self.open_event.emit(self.le.text())

    def add_dialog(self):
        if self.file_dialog.exec_():
            file_names = translate_to_norm_view(
                self.file_dialog.selectedFiles())
            self.add_event.emit(file_names)

    def save_dialog(self):
        self.save_message_box.save_query()

    @QtCore.pyqtSlot(str)
    def save_database(self, new_db_name: str):
        self.save_event.emit(new_db_name)

    def clear_dialog(self):
        self.clear_event.emit()


class MainWindowWithFileMenu(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)

        menu_bar = self.menuBar()
        file_menu = FileMenu(self)
        file_menu.open_dir_event.connect(self.open_dir)
        file_menu.add_dir_event.connect(self.add_dir)
        file_menu.open_files_event.connect(self.open_files)
        file_menu.add_files_event.connect(self.add_files)

        album_menu = AlbumMenu(self)
        album_menu.add_event.connect(self.add_album)
        album_menu.open_event.connect(self.open_album)
        album_menu.save_event.connect(self.save_database)
        album_menu.clear_event.connect(self.clear_database)

        menu_bar.addMenu(file_menu)
        menu_bar.addMenu(album_menu)

    @QtCore.pyqtSlot(list)
    def open_dir(self, file_names):
        print("Open dir event has "
              "thrown with: \n\t{}".format("\n\t".join(map(str, file_names))))

    @QtCore.pyqtSlot(list)
    def add_dir(self, file_names):
        print("Add dir event has "
              "thrown with: \n\t{}".format("\n\t".join(map(str, file_names))))

    @QtCore.pyqtSlot(list)
    def open_files(self, file_names):
        print("Open files event has "
              "thrown with: \n\t{}".format("\n\t".join(map(str, file_names))))

    @QtCore.pyqtSlot(list)
    def add_files(self, file_names):
        print("Add files event has "
              "thrown with: \n\t{}".format("\n\t".join(map(str, file_names))))

    @QtCore.pyqtSlot(list)
    def open_album(self, album_names):
        print("Open albums event has "
              "thrown with: \n\t{}".format("\n\t".join(map(str, album_names))))

    @QtCore.pyqtSlot(str)
    def add_album(self, album_name):
        print("Open albums event has "
              "thrown with: \n\t{}".format(album_name))

    @QtCore.pyqtSlot(str)
    def save_database(self, new_db_name: str):
        pass

    @QtCore.pyqtSlot()
    def clear_database(self):
        pass
