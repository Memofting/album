#!/usr/bin/env python3
import sys
import os
import functools
from PyQt5 import QtCore, QtWidgets
from DBAlbum import AlbumDataBase
from Album import Album
from AlbumWidget import AlbumWidget
from AlbumScrollArea import AlbumScrollArea
from ScrollArea import MyScrollArea
from MainWindowWithFileMenu import MainWindowWithFileMenu
from ImageInfoWidget import ImageInfoWidget


def not_empty_second_arg(func):
    @functools.wraps(func)
    def wrapper(self, file_names, *args, **kwargs):
        if file_names:
            func(self, file_names, *args, **kwargs)
    return wrapper


class MyMainWindow(MainWindowWithFileMenu):
    def __init__(self,
                 small_image_size=(100, 100),
                 main_image_size=(600, 600), parent=None):
        super().__init__(parent)

        self.database = AlbumDataBase("LocalDatabase")
        self.small_image_size = small_image_size
        self.main_image_size = main_image_size
        self.layout = QtWidgets.QGridLayout()

        self.first_window = ImageInfoWidget(self)

        self.scroll_area = MyScrollArea(path=MyScrollArea.LOCAL_IMAGES_PATH,
                                        img_size=self.small_image_size,
                                        elements_on_line=4,
                                        start_lines=50,
                                        upload_lines=50,
                                        album_name="LocalAlbum",
                                        parent_database=self.database)
        self.scroll_area.clicked_image.connect(self.show_image)

        self.albums = []
        self.albums_names = set()
        self.albums_scroll_area = AlbumScrollArea()

        self.layout.addWidget(self.first_window, 0, 0)
        self.layout.addWidget(self.scroll_area, 0, 1)
        self.layout.addWidget(self.albums_scroll_area, 0, 2)

        self.layout.setColumnStretch(0, 4)
        self.layout.setColumnStretch(1, 3)
        self.layout.setColumnStretch(2, 2)

        self.window = QtWidgets.QWidget()
        self.window.setLayout(self.layout)
        self.setCentralWidget(self.window)

        center = QtWidgets.QDesktopWidget().availableGeometry().center()
        self.resize(QtCore.QSize(center.x(), center.y()))
        self.show()
        self.upload_database_entity()

    @QtCore.pyqtSlot(str)
    def show_image(self, image_name):
        for _, image in MyScrollArea.get_scaled_images_with_names(
                [image_name], *self.main_image_size):
            self.first_window.upload_image(image)
            break

    @QtCore.pyqtSlot(list)
    @not_empty_second_arg
    def open_dir(self, file_names: list):
        """
        Open directory content as new scroll_area object
        :param file_names: list that contains 1 path to directory
        :return: None
        """
        self.replace_current_scroll_area_with_new(
            album=self.scroll_area.album,
            parent_database=self.database
        )
        self.scroll_area.clear_albums_entity()
        self.add_dir(file_names)
        self.scroll_area.show_albums_entity()

    @QtCore.pyqtSlot(list)
    @not_empty_second_arg
    def add_dir(self, file_names):
        """
        Add directory content to the end of current elements
        :param file_names: list that contains 1 path to directory
        :return: None
        """
        self.scroll_area.add_dir(file_names[0])
        self.scroll_area.upload_thread.start()
        self.connect_albums_with_clicking()

    @QtCore.pyqtSlot(list)
    @not_empty_second_arg
    def open_files(self, file_names):
        """
        Open directory content as new scroll_area object
        :param file_names: list that contains 1 path to directory
        :return: None
        """
        self.replace_current_scroll_area_with_new(
            album=self.scroll_area.album,
            parent_database=self.database)
        self.scroll_area.clear_albums_entity()
        self.add_files(file_names)
        self.scroll_area.show_albums_entity()

    @QtCore.pyqtSlot(list)
    @not_empty_second_arg
    def add_files(self, file_names):
        """
        Add files to the end of current elements
        :param file_names: list that contains set of images' path
        :return: None
        """
        self.scroll_area.add_images(file_names)
        self.scroll_area.upload_thread.start()
        self.connect_albums_with_clicking()

    @QtCore.pyqtSlot(str)
    @not_empty_second_arg
    def open_album(self, album_name):
        """
        Create new album and create album_state file.
        :param album_name: Entered new name for album
        :return: None
        """
        self.replace_current_scroll_area_with_new(
            album=album_name,
            parent_database=self.database)
        self.update_albums_scroll(self.scroll_area.album)
        self.scroll_area.show_albums_entity()

    @QtCore.pyqtSlot(list)
    @not_empty_second_arg
    def add_album(self, database_name: list):
        """
        Add albums from another database to current.
        Result is merging from database_name to current.
        :param database_name: Name of database.
        :return: None
        """
        database_name = os.path.splitext(database_name[0])[0]
        new_database = AlbumDataBase(database_name)
        self.database.merge(new_database)
        self.upload_database_entity()

    @QtCore.pyqtSlot(Album)
    def create_new_scroll_with_album(self, album: Album):
        self.replace_current_scroll_area_with_new(
            album=album,
            parent_database=self.database)
        self.scroll_area.show_albums_entity()
        self.connect_albums_with_clicking()

    def replace_current_scroll_area_with_new(
            self,
            album=MyScrollArea.DEFAULT_ALBUM_NAME,
            parent_database=AlbumDataBase("LocalDatabase"),
            path=''):
        new_scroll = MyScrollArea(path=path,
                                  img_size=self.small_image_size,
                                  elements_on_line=4,
                                  start_lines=50,
                                  upload_lines=50,
                                  album_name=album,
                                  parent_database=parent_database)
        new_scroll.clicked_image.connect(self.show_image)
        new_scroll.create_new_with_album.connect(
            self.create_new_scroll_with_album)
        self.layout.replaceWidget(self.scroll_area, new_scroll)
        self.scroll_area.stop_uploading()
        self.scroll_area.deleteLater()
        self.scroll_area = new_scroll

    def update_albums_scroll(self, album_widget: AlbumWidget):
        self.albums.append(AlbumWidget(album_widget))
        new_album_scroll = AlbumScrollArea(*self.albums)

        self.layout.replaceWidget(self.albums_scroll_area, new_album_scroll)
        self.albums_scroll_area.deleteLater()
        self.albums_scroll_area = new_album_scroll
        self.connect_albums_with_clicking()

    def connect_albums_with_clicking(self):
        for album in self.albums:
            album.clicked.connect(self.scroll_area.upload_image_icons,
                                  QtCore.Qt.QueuedConnection)
            self.albums_names.add(album.name)

    def upload_database_entity(self):
        info = self.database.get_albums_info()
        for albums_name, albums_info in info.items():
            if albums_name not in self.albums_names:
                self.replace_current_scroll_area_with_new(
                    album=albums_name,
                    parent_database=self.database)
                self.update_albums_scroll(self.scroll_area.album)
                self.albums_names.add(albums_name)
        self.scroll_area.show_albums_entity()

    @QtCore.pyqtSlot(str)
    def save_database(self, new_db_name: str):
        self.database.save(new_path=new_db_name)

    @QtCore.pyqtSlot()
    def clear_database(self):
        self.database.clear()
        self.albums.clear()
        self.albums_names.clear()
        self.replace_current_scroll_area_with_new()
        new_album_scroll = AlbumScrollArea()

        self.layout.replaceWidget(self.albums_scroll_area, new_album_scroll)
        self.albums_scroll_area.deleteLater()
        self.albums_scroll_area = new_album_scroll
        self.connect_albums_with_clicking()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyMainWindow()
    sys.exit(app.exec_())
