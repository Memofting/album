#!usr/bin/env python3
import shelve
import os
import time


class ImageInfo:
    def __init__(self, albums_names=None, image_bytes=None, ext="JPG"):
        """
        albums_names: list of str
        clusters: list of str
        """
        self.image_bytes = image_bytes
        self.albums_names = {}
        self.clusters = set()
        self.ext = ext
        if albums_names is not None:
            for album in albums_names:
                self.add_albums_names(album=album)

    def merge(self, other_image_info: 'ImageInfo'):
        """
        Merge information about albums and clusters.
        Image_bytes and ext also will change. Information
        that will be in result is information with more
        recent time of changing.
        """
        self.image_bytes = other_image_info.image_bytes
        self.ext = other_image_info.ext
        self.update_albums_names(other_image_info.albums_names)
        self.update_clusters(other_image_info.clusters)

    def update_albums_names(self, new_albums_names):
        """
        Обновляет список альбомов в соответствии ос временем добавления в них.
        """
        for album_name, new_time in new_albums_names.items():
            cur_time = self.albums_names.get(album_name, -1)
            self.albums_names.update({album_name: max(cur_time, new_time)})

    def total_update_image_info(self, **kvargs):
        for name in list(filter(lambda n: not n.startswith('__'), dir(self))):
            if kvargs.get(name, None) is not None:
                setattr(self, name, kvargs[name])

    def update_clusters(self, clusters):
        pass

    def add_albums_names(self, album, cur_time=time.time()):
        """
        Добавляет картинки в альбом с учетом переданного времени, если
        время не было передано, то берется текущее время.
        """
        self.albums_names.update({album: cur_time})

    def remove_albums_names(self, albums_name: str):
        self.albums_names.pop(albums_name, None)

    def __eq__(self, other_image_info):
        """
        Simplified version of comparing two ImageInfo for testing requires.
        """
        if not isinstance(other_image_info, ImageInfo):
            return False
        elif self.image_bytes != other_image_info.image_bytes:
            return False
        elif self.ext != other_image_info.ext:
            return False
        cur_albums = sorted(self.albums_names.keys())
        other_albums = sorted(other_image_info.albums_names.keys())
        if cur_albums != other_albums:
            return False
        return True


class AlbumDataBase:
    """
    Реализует сущность бащы данных, для работы с картинками
    """
    _BASE_DIR = os.path.join(".", "albums_databases")
    ORDER_NAME = "order"
    IMAGES_NAME = "images"

    def __init__(self, name):
        """
        name - путь до базы данных. Так как используется
        shelve, то в пути уберается расширение. Если это новая база,
        то name содержит имя для нее.
        """
        if isinstance(name, AlbumDataBase):
            self.full_filename = name.full_filename
        else:
            os.makedirs(os.path.abspath(self._BASE_DIR), exist_ok=True)
            full_name = os.path.join(
                os.path.abspath(self._BASE_DIR),
                name)
            raw_name = os.path.splitext(name)[0]
            if os.path.exists(raw_name):
                if not self.create_if_correct(raw_name):
                    raise ValueError("Incorrect filename: {}".format(name))
                self.full_filename = raw_name
            else:
                if not self.create_if_correct(full_name):
                    raise ValueError("Incorrect new name: {}".format(name))
                self.full_filename = full_name

    @staticmethod
    def get_base_dir():
        return os.path.abspath(AlbumDataBase._BASE_DIR)

    @staticmethod
    def create_if_correct(name):
        """
        Возвращает результат создания файла с базой данных
        res: bool
        """
        try:
            with shelve.open(name, 'c'):
                pass
        except Exception:
            return False
        else:
            return True

    def get_albums_info(self):
        """
        Получает информацию со всех альбомов.
        db: dict({
            image_name: ImageInfo
        })
        result: dict({
                    albums_name: dict({
                        order: list(images_names),
                        images: dict({image_name: ImageInfo})
                    })
                })
        """
        result = dict()
        with shelve.open(self.full_filename, 'c') as db:
            for image_name, image_info in db.items():
                albums_names_info = image_info.albums_names
                for albums_name, cur_time in albums_names_info.items():
                    result.setdefault(albums_name, {})
                    result[albums_name].setdefault(self.ORDER_NAME, [])
                    result[albums_name].setdefault(self.IMAGES_NAME, dict())
                    result[albums_name][self.ORDER_NAME].append(
                        (cur_time, image_name))
                    result[albums_name][self.IMAGES_NAME].update(
                        {image_name: image_info})
        for albums_name, album_info in result.items():
            album_info[self.ORDER_NAME] = [
                pair[1] for pair in sorted(album_info[self.ORDER_NAME])]
        return result

    def add_image(self, image_name: str,
                  albums_name: str,
                  image_bytes: bytes = None,
                  ext: str = None):
        """
        Добавляет картинку в альбом.
        """

        with shelve.open(self.full_filename, 'c') as db:
            image_info = db.get(image_name, ImageInfo())
            image_info.add_albums_names(
                album=albums_name, cur_time=time.time())
            image_info.total_update_image_info(
                image_bytes=image_bytes, ext=ext)
            db.update({image_name: image_info})

    def remove_image(self, image_name: str, albums_name=None):
        """
        Удаляет картинку из альбома. Если альбом не указан, то удаляет саму
        запись о картинке из альбома.
        """
        with shelve.open(self.full_filename, 'c') as db:
            if albums_name is not None:
                image_info = db.get(image_name, ImageInfo())
                image_info.remove_albums_names(albums_name)
                db.update({image_name: image_info})
            else:
                db.pop(image_name, None)

    def merge(self, other_albums_info):
        """
        Сливает информацию с двух баз данных в одну. (текущую).
        В случае информации об одинаковых файлах, старая информация
        заменяется на новую.
        """
        with shelve.open(self.full_filename, 'c') as db:
            other_db = AlbumDataBase(other_albums_info)
            other_albums_info = other_db.get_albums_info()
            for album_name, album_info in other_albums_info.items():
                for image_name in album_info[self.ORDER_NAME]:
                    image_info = db.get(image_name, ImageInfo())
                    image_info.merge(album_info[self.IMAGES_NAME][image_name])
                    db.update({image_name: image_info})

    def clear(self, albums_name: str = None):
        """
        Чистит содержимое базы дынных. Если указано имя альбома,
        то чистит информацию, связанную только с этим альбомом.
        """
        with shelve.open(self.full_filename, 'c') as db:
            if albums_name is not None:
                for image_name, image_info in db.items():
                    image_info.remove_albums_names(albums_name=albums_name)
                    if not image_info.albums_names:
                        db.pop(image_name, None)
                    else:
                        db.update({image_name: image_info})
            else:
                db.clear()

    def save(self, new_path: str):
        """
        Сохраняет содержимое базы данных в файл, с переданным именем.
        :param new_path: файл с новым именем базы данных
        :return: None
        """
        with shelve.open(self.full_filename, 'c') as db:
            with shelve.open(new_path, 'c') as other_db:
                for image, info in db.items():
                    other_db.update({image: info})
