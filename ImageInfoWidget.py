#!/usr/bin/env python3
from PyQt5 import QtCore, QtWidgets, QtGui
import sys


def empty_func(*args, **kargs):
    pass


class ScaledImage(QtWidgets.QLabel):
    MIN_SIZE_PRODUCT = 10e2
    MAX_SIZE_PRODUCT = 10e6

    def __init__(self, image_name, parent=None):
        super().__init__(parent)

        if isinstance(image_name, str):
            self.original_image = QtGui.QImage(image_name)
        elif isinstance(image_name, QtGui.QImage):
            self.original_image = image_name

        if not self.original_image.isNull():
            self.aspect_ratio = (
                    self.original_image.width() /
                    self.original_image.height())
            self.setFrameStyle(QtWidgets.QFrame.Box |
                               QtWidgets.QFrame.Panel)
            self.setAlignment(QtCore.Qt.AlignCenter)
            self.setPixmap(QtGui.QPixmap.fromImage(self.original_image))
            self.setSizePolicy(
                QtWidgets.QSizePolicy.Minimum,
                QtWidgets.QSizePolicy.Minimum
            )

    def wheelEvent(self, a0: QtGui.QWheelEvent):
        image = self.pixmap().toImage()
        delta = a0.angleDelta().y() // 3
        width = image.width() + delta
        height = width / self.aspect_ratio
        if (self.MIN_SIZE_PRODUCT < width*height <
           self.MAX_SIZE_PRODUCT and width + height > 0):
            image = QtGui.QImage(
                self.original_image.scaled(
                    width, round(height),
                    transformMode=QtCore.Qt.SmoothTransformation))
            self.setPixmap(QtGui.QPixmap.fromImage(image))


class ImageInfoWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.description = QtWidgets.QLabel(self)
        self.description.setFrameStyle(QtWidgets.QFrame.Box |
                                       QtWidgets.QFrame.Panel)
        self.description.setAlignment(QtCore.Qt.AlignLeft |
                                      QtCore.Qt.AlignTop)

        self.image_widget = ScaledImage("", self)
        self.scroll_area = QtWidgets.QScrollArea(self)
        self.scroll_area.setWidget(self.image_widget)
        self.scroll_area.setVerticalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOn
        )
        self.scroll_area.setHorizontalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOn
        )
        self.scroll_area.wheelEvent = empty_func
        self.scroll_area.setWidgetResizable(True)

        self.main_layout = QtWidgets.QVBoxLayout(self)
        self.main_layout.addWidget(self.scroll_area, stretch=2)
        self.main_layout.addWidget(self.description, stretch=1)

    def upload_image(self, image_name):
        self.image_widget.deleteLater()
        self.image_widget = ScaledImage(image_name, self)
        self.scroll_area.setWidget(self.image_widget)
