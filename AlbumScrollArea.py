from PyQt5 import QtCore, QtWidgets


class AlbumScrollArea(QtWidgets.QScrollArea):
    def __init__(self, *album_widgets):
        super().__init__()
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.vbox = QtWidgets.QVBoxLayout()
        self.vbox.setAlignment(QtCore.Qt.AlignCenter)
        for widget in album_widgets:
            self.vbox.addWidget(widget)
        self.widget = QtWidgets.QWidget()
        self.widget.setLayout(self.vbox)
        self.setWidget(self.widget)
